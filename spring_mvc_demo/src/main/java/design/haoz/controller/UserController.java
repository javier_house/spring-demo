package design.haoz.controller;

import design.haoz.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Javier House
 */
@Controller
@RequestMapping("/user")
public class UserController {
    /**
     * 转发测试
     * @return
     */
    @RequestMapping("/test")
    private String test() {
        return "/WEB-INF/test.jsp";
    }

    /**
     * 对象测试
     * @return
     */
    @RequestMapping("/test1")
    @ResponseBody
    private User test1() {
        User user = new User();
        user.setName("中文测试");
        user.setId(1);
        user.setAge(18);
        return user;
    }

    /**
     * 集合测试
     * @return
     */
    @RequestMapping("/test2")
    @ResponseBody
    private List<User> test2() {
        User user = new User();
        user.setName("中文测试");
        user.setId(1);
        user.setAge(18);
        User user1 = new User();
        user1.setName("中文测试");
        user1.setId(1);
        user1.setAge(18);
        ArrayList<User> users = new ArrayList<>();
        users.add(user);
        users.add(user1);
        return users;
    }
}
