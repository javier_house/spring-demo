# [Spring-demo](https://gitee.com/javier_house/Spring-demo)

## spring_mvc_demo:

​	springmvc的demo

​	静态资源放行的模式

​	使用fastjson

### 软件需求

- JDK1.8
  - 在1.8环境下搭建,不保证低版本运行
- Maven3.0+

### 本地部署

- 通过git下载源码

  

- 编译项目,将controller下的war包放到tomcat的webapps下运行taomcat


#### 喜欢欢迎给个star，有问题可以给我发邮件javier_house@qq.com