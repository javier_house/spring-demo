package design.haoz.exam2security.pojo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * 用户
 */
@Table(name="t_user")
@Data
public class User implements Serializable{
    /**
     * 主键
     */
    @Id
    private Integer id;
    /**
     * 用户名，唯一
     */
    private String username;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 密码
     */
    private String password;
    /**
     * 是否启用
     */
    private Integer enabled;
    /**
     * 帐户未过期
     */
    @Column(name = "accountNonExpired")
    private Integer accountNonExpired;
    /**
     * 凭据未过期
     */
    @Column(name = "credentialsNonExpired")
    private Integer credentialsNonExpired;
    /**
     * 状态
     */
    @Column(name = "createDate")
    private Date createDate;
    /**
     * 联系电话
     */
    @Column(name = "lastLoginTime")
    private Date lastLoginTime;

    /**
     * 对应角色集合
     */
    private Set<Role> roles = new HashSet<>(0);

}
