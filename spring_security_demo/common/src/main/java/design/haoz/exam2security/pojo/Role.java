package design.haoz.exam2security.pojo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 角色
 */
@Table(name="t_role")
@Data
public class Role implements Serializable {
    @Id
    private Integer id;
    /**
     * 角色名称
     */
    @Column(name = "roleName")
    private String roleName;
    /**
     * 角色标签
     */
    @Column(name = "roleDesc")
    private String roleDesc;
    /**
     * 对应用户名
     */
    private Set<User> users = new HashSet<>(0);
    /**
     * 对应角色权限
     */
    private Set<Permission> permissions = new HashSet<>(0);
}
