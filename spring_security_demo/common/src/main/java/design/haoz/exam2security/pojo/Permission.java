package design.haoz.exam2security.pojo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 权限
 */
@Table(name="t_permission")
@Data
public class Permission implements Serializable{
    @Id
    private Integer id;
    /**
     * 权限名称
     */
    @Column(name = "permName")
    private String permName;
    /**
     * 权限标签
     */
    @Column(name = "permTag")
    private String permTag;
    /**
     * 对应角色集合
     */
    private Set<Role> roles = new HashSet<>(0);
}
