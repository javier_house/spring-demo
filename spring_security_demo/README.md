# [Spring-demo](https://gitee.com/javier_house/Spring-demo)

## spring_security_demo:

​	security权限控制的demo

​	访问后缀使用.php,如需更改,在controller目录下：

​				src/main/webapp/index.html

​				src/main/webapp/WEB-INF/web.xml

​				src/main/webapp/WEB-INF/index.jsp

​				src/main/resources/spring-security.xml

​								的.php替换为你想要的后缀既可,注意：不要拦截html等后缀

### 软件需求

- JDK1.8
  - 在1.8环境下搭建,不保证低版本运行
- MySQL5.5+
- Maven3.0+
- tomcat8.5+
  - 注：因使用较新的依赖,不要使用maven的tomcat7插件,会报500错误

### 本地部署

- 通过git下载源码

- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法

- 创建数据库，数据库编码为UTF-8

- 执行sql/spring_security_demo.sql文件，初始化数据【按需导入表结构及数据】

- 修改spring_security_demo\dao\src\main\resources\jdbc.properties文件，更新MySQL连接地址,账号和密码

  

- 编译项目,将controller下的war包放到tomcat的webapps下运行taomcat

- 账号密码：admin/123456

#### 喜欢欢迎给个star，有问题可以给我发邮件javier_house@qq.com