package design.haoz.exam2security.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 用户登录
 * @author JavierHouse
 */
@Controller
public class LoginController {
    /**
     * 当用户已经登录的情况下直接跳转到首页
     * @return
     */
    @RequestMapping("/login")
    public String login(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth instanceof AnonymousAuthenticationToken){
            //已经登录重定向
            return "redirect:index";
        }else{
            //没有登录的先登录
            return "/WEB-INF/login.jsp";
        }
    }
}
