package design.haoz.exam2security.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 商品列表
 *
 * @author JavierHouse
 */
@Controller
public class ListController {
    /**
     * 首页
     *
     * @return
     */
    @RequestMapping("/index")
    public String index(ModelMap map) {
        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User)
                        SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        map.addAttribute("username",username);
        //重定向到首页
        return "/WEB-INF/index.jsp";
    }

    /**
     * 添加
     *
     * @return
     */
    @RequestMapping("/add")
    @PreAuthorize("hasAuthority('ROLE_PRODUCT_ADD')")
    public String add() {
        //添加页面
        return "/WEB-INF/pages/add.jsp";
    }

    /**
     * 删除
     *
     * @return
     */
    @RequestMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_PRODUCT_DELETE')")
    public String delete() {
        //删除页面
        return "/WEB-INF/pages/delete.jsp";
    }

    /**
     * 添加
     *
     * @return
     */
    @RequestMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_PRODUCT_UPDATE')")
    public String update() {
        // 更新页面
        return "/WEB-INF/pages/update.jsp";
    }

    /**
     * 列表
     *
     * @return
     */
    @RequestMapping("/list")
    @PreAuthorize("hasAuthority('ROLE_PRODUCT_LIST')")
    public String list() {
        //列表页面
        return "/WEB-INF/pages/list.jsp";
    }

}
