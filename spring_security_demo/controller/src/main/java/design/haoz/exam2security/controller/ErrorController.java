package design.haoz.exam2security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 错误页面
 * @author Javice House
 */
@Controller
public class ErrorController {
    /**
     * 错误
     *
     * @return
     */
    @RequestMapping("/error")
    public String err() {
        //错误页面
        return "/WEB-INF/error.jsp";
    }
}
