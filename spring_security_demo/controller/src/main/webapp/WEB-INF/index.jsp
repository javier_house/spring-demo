<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<html>
<head>
    <title>商品首页</title>
</head>
<body>
欢迎用户，${username}<br/>
<a href="./logout">退出登录</a><br>
以下是商品的功能：<br/>

<a href="${pageContext.request.contextPath}/list.php">商品列表</a><br/>


<a href="${pageContext.request.contextPath}/add.php">商品添加</a><br/>


<a href="${pageContext.request.contextPath}/update.php">商品修改</a><br/>


<a href="${pageContext.request.contextPath}/delete.php">商品删除</a><br/>

</body>
</html>
