<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>登录页面</title>
</head>
<body>
<h3>登录页面</h3>

<span id="notice"></span>
<form method="post" id="loginForm" action="./login">
    用户名:<input type="text" name="username" value=""/><br/>
    密码:<input type="password" name="password" value=""/><br/>

    <input type="submit" id="loginBtn" value="登录"/>
</form>
</body>
<script type="application/javascript" charset="UTF-8">
    window.onload = function () {
        let href = location.href;
        let split = href.split("?");
        if (split[1] === "error"){
            document.getElementById("notice").innerHTML = '<font color="red">用户名或者密码错误</font>';
        }else{
            document.getElementById("notice").innerHTML = '<font color="green">欢迎登录</font>';
        }
    }
</script>
</html>
