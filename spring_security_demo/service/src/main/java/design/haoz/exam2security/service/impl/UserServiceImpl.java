package design.haoz.exam2security.service.impl;

import design.haoz.exam2security.dao.PermissionDao;
import design.haoz.exam2security.dao.RoleDao;
import design.haoz.exam2security.dao.UserDao;
import design.haoz.exam2security.pojo.Permission;
import design.haoz.exam2security.pojo.Role;
import design.haoz.exam2security.pojo.User;
import design.haoz.exam2security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author JavierHouse
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PermissionDao permissionDao;

    @Override
    public User findByUsername(String username) {
        User user = new User();
        user.setUsername(username);
        user = userDao.selectOne(user);
        if (user == null){
            return null;
        }
        Integer userId = user.getId();
        Set<Role> roleSet = roleDao.findByUserId(userId);
        Integer roleId;
        Set<Permission> permissionSet;
        for (Role role : roleSet) {
            roleId = role.getId();
            permissionSet = permissionDao.findByRoleId(roleId);
            role.setPermissions(permissionSet);
        }
        user.setRoles(roleSet);
        return user;
    }
}
