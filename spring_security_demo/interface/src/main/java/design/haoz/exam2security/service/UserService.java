package design.haoz.exam2security.service;

import design.haoz.exam2security.pojo.User;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author JavierHouse
 */
@Transactional(readOnly = true)
public interface UserService {
    /**
     * 根据用户名查询用户是否存在
     * @param username
     * @return
     */
    public User findByUsername(String username);
}
