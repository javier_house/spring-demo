package design.haoz.exam2security.dao;

import design.haoz.exam2security.pojo.User;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author JavierHouse
 */
public interface UserDao extends Mapper<User> {
}
