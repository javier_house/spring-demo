package design.haoz.exam2security.dao;

import design.haoz.exam2security.pojo.Permission;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.Set;

/**
 * @author JavierHouse
 */
public interface PermissionDao extends Mapper<Permission> {
    /**
     * 根据角色id查询权限
     * @param roleId
     * @return
     */
    @Select("SELECT p.* FROM t_permission p ,t_role_permission rp WHERE p.id = rp.perm_id AND rp.role_id = #{roleId}")
    public Set<Permission> findByRoleId(int roleId);
}
