package design.haoz.exam2security.dao;

import design.haoz.exam2security.pojo.Role;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.Set;

/**
 * @author JavierHouse
 */
public interface RoleDao extends Mapper<Role> {
    /**
     * 根据用户id查询角色
     *
     * @param userId
     * @return
     */
    @Select("SELECT r.* FROM t_role r ,t_user_role ur WHERE r.id = ur.role_id AND ur.user_id = #{userId}")
    public Set<Role> findByUserId(int userId);
}
