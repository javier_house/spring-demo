package design.haoz.web.controller;

import com.alibaba.fastjson.JSONObject;
import design.haoz.domain.User;
import design.haoz.service.UserService;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Javier House
 */
@WebServlet("/user")
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //查询链接http://localhost/user?id=4
        //设置相应数据为json
        resp.setContentType("application/json;charset=utf-8");
        //获取对象
        WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(req.getServletContext());
        UserService service = (UserService) applicationContext.getBean("userService");
        //取得参数
        String id = req.getParameter("id");
        //查询
        User user = service.findById(Integer.parseInt(id));
        //转为json
        Object o = JSONObject.toJSON(user);
        String s = o.toString();
        //返回json数据
        resp.getWriter().write(s);
    }
}
