package design.haoz.service;

import design.haoz.domain.User;

/**
 * @author Javier House
 */
public interface UserService {
    //根据id查询
    public User findById(int id);
}
