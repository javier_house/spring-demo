package design.haoz.dao;

import design.haoz.domain.User;
import org.apache.ibatis.annotations.Select;

/**
 * @author Javier House
 */

public interface UserDao  {
    /**
     * 根据id查询的dao
     * @param id
     * @return
     */
    @Select("select uid id,username,sex,birthday,address from t_user where uid = #{id}")
    public User findById(int id);
}
